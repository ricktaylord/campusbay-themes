<?php
    /*
     *      OSCLass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2010 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    if( !OC_ADMIN ) {
        if( !function_exists('add_close_button_fm') ) {
            function add_close_button_fm($message){
                return $message.'<a class="close">×</a>' ;
            }
            osc_add_filter('flash_message_text', 'add_close_button_fm') ;
        }
        if( !function_exists('add_close_button_action') ) {
            function add_close_button_action(){
                echo '<script type="text/javascript">';
                    echo '$(".FlashMessage .close").click(function(){';
                        echo '$(this).parent().hide();';
                    echo '});';
                echo '</script>';
            }
            osc_add_hook('footer', 'add_close_button_action') ;
        }
    }

    if( !function_exists('add_logo_header') ) {
        function add_logo_header() {
             $html = '<img border="0" alt="' . osc_page_title() . '" src="' . osc_current_web_theme_url('images/logo.jpg') . '">';
             $js   = "<script>
                          $(document).ready(function () {
                              $('#logo').html('".$html."');
                          });
                      </script>";

             if( file_exists( WebThemes::newInstance()->getCurrentThemePath() . "images/logo.jpg" ) ) {
                echo $js;
             }
        }

        osc_add_hook("header", "add_logo_header");
    }

    if( !function_exists('cbay_admin_menu') ) {
        function cbay_admin_menu() {
            echo '<h3><a href="#">'. __('Modern theme','modern') .'</a></h3>
            <ul>
                <li><a href="' . osc_admin_render_theme_url('oc-content/themes/modern/admin/admin_settings.php') . '">&raquo; '.__('Settings theme', 'modern').'</a></li>
            </ul>';
        }

        osc_add_hook('admin_menu', 'modern_admin_menu');
    }

    if( !function_exists('cbay_sort_subcategories'))  {
	function cbay_sort_subcategories()  {
		function cmp_category($a, $b)
		{
			if($a['i_num_items'] == $b['i_num_items'])
				return 0;
			return ($a['i_num_items'] > $b['i_num_items']) ? -1 : 1;
		}
		$sorted=array();

		foreach(osc_get_categories() as $i=>$root)
		{
			usort($root['categories'], "cmp_category");
			$sorted[$i] = $root;
		}	
		View::newInstance()->_exportVariableToView('categories',$sorted);	
    	}
    }

    if (!function_exists('cbay_first_category_select')) {
	function cbay_first_category_select($name = 'sCategory', $category = null, $default_str = null)  {
		$first_cat = current(Category::newInstance()->toTree());
		return CBayItemForm::category_select($first_cat['categories'], $category, $default_str, $name);
	}
    }

    function cbay_root_category_select($root, $name = 'sCategory', $category = null, $default_str = null)  {
	$first_cat = current(Category::newInstance()->toTree());
	return CBayItemForm::category_select(Category::newInstance()->toSubTree($root['pk_i_id']), $category, $default_str, $name);
    }

    if (!function_exists('cbay_root_categories_select')) {
	function cbay_root_categories_select($name = 'sCategory', $category = null, $default_str = null)  {
        	return CategoryForm::category_select(Category::newInstance()->findRootCategoriesEnabled(), $category, $default_str, $name) ;
	}
    }

    function cbay_post_url_in_category_id($id) {
        if ( osc_rewrite_enabled() ) {
                $path = osc_base_url() . osc_get_preference('rewrite_item_new') . '/' . $id;
            } else {
                $path = sprintf(osc_base_url(true) . '?page=item&action=item_add&catId=%d', $id) ;
            }
        return $path ;
    }

   

    function cbay_root_category_by_slug($slug) {
	$root_categories = Category::newInstance()->findRootCategoriesEnabled();
	foreach($root_categories as $cat)
	{
		if($cat['s_slug']==$slug) return $cat;
	}
    }
   

    function cbay_item_category() {
       return cbay_root_category_by_slug("for-sale");
    }   
    function cbay_item_category_id() {
	$cat = cbay_root_category_by_slug("for-sale");
	return $cat['pk_i_id'];
    }

    function cbay_service_category_id() {
	$cat = cbay_service_category();
	return $cat['pk_i_id'];
    }

    function cbay_service_category() {
       return cbay_root_category_by_slug("services");
    }
  

    function cbay_item_post_url() {
	$cat = cbay_item_category();
        return cbay_post_url_in_category_id($cat['pk_i_id']);
    }

    function cbay_service_post_url() {
	$cat = cbay_service_category();
	return cbay_post_url_in_category_id($cat['pk_i_id']);

    }



    function cbay_item_select($name = 'sCategory', $category = null, $default_str = null)  {
	return cbay_root_category_select(cbay_item_category(), $name, $category, $default_str);
    }


    function cbay_service_select($name = 'sCategory', $category = null, $default_str = null)  {
	return cbay_root_category_select(cbay_service_category(), $name, $category, $default_str);
    }

    function cbay_root_for_category_id($id)  {
	return Category::newInstance()->findRootCategory($id);
    }

    function cbay_is_item_shop() {
	return osc_is_this_category('shop', osc_item_category_id());
    }
  
    function cbay_is_item_service() {
        return osc_is_this_category('advanced-ad-management', osc_item_category_id());
    }

	function _cbay_prune_branches($branches = null, &$out)
        {
            if($branches!=null) {
                foreach($branches as $branch) {
                    if(!in_array($branch['pk_i_id'], $out)) {
                        $out[] = $branch['pk_i_id'] ;
                        if(isset($branch['categories'])) {
                            $list = _cbay_prune_branches($branch['categories'], $out);
                        }
                    }
                }
            }
        }

        /**
         * Add categories to the search
         *
         * @access public
         * @since unknown
         * @param mixed $category
         */
        function cbay_get_subcategory_ids($category = null)
        {
            if( $category == null ) {
                return '' ;
            }

            if( !is_numeric($category) ) {
                $category  = preg_replace('|/$|','',$category);
                $aCategory = explode('/', $category) ;
                $category  = Category::newInstance()->findBySlug($aCategory[count($aCategory)-1]) ;

                if( count($category) == 0 ) {
                    return '' ;
                }

                $category  = $category['pk_i_id'] ;
            }
	    $out_categories = array();

            $tree = Category::newInstance()->toSubTree($category) ;
            if( !in_array($category, $out_categories) ) {
                $out_categories[] = $category ;
            }
            _cbay_prune_branches($tree, $out_categories);
	    return $out_categories;
        }


    function cbay_count_latest_items($total_latest_items = null, $category = null) {
        if ( !View::newInstance()->_exists('latestItems') ) {
	    osc_run_hook('search_conditions', array());
            if( !is_numeric($total_latest_items) ) {
                $total_latest_items = osc_max_latest_items() ;
            }
            View::newInstance()->_exportVariableToView('latestItems', Search::newInstance()->getLatestItems($total_latest_items, cbay_get_subcategory_ids($category))) ;
        };
        return (int) View::newInstance()->_count('latestItems') ;
    }


    /**
     * Gets next item of last items
     *
     * @return array
     */
    function cbay_has_latest_items($total_latest_items = null, $category = null) {
        if ( !View::newInstance()->_exists('latestItems') ) {
            if( !is_numeric($total_latest_items) ) {
                $total_latest_items = osc_max_latest_items() ;
            }
            osc_run_hook("search_conditions");
            View::newInstance()->_exportVariableToView('latestItems', Search::newInstance()->getLatestItems($total_latest_items, cbay_get_subcategory_ids($category)));
        }
        if ( View::newInstance()->_exists('resources') ) {
            View::newInstance()->_erase('resources') ;
        }
        if ( View::newInstance()->_exists('item_category') ) {
            View::newInstance()->_erase('item_category') ;
        }
        if ( View::newInstance()->_exists('metafields') ) {
            View::newInstance()->_erase('metafields') ;
        }
        if(View::newInstance()->_get('itemLoop')!='latest') {
            View::newInstance()->_exportVariableToView('oldItem', View::newInstance()->_get('item'));
            View::newInstance()->_exportVariableToView('itemLoop', 'latest');
        }
        $item = View::newInstance()->_next('latestItems') ;
        if(!$item) {
            View::newInstance()->_exportVariableToView('item', View::newInstance()->_get('oldItem'));
            View::newInstance()->_exportVariableToView('itemLoop', '');
        } else {
            View::newInstance()->_exportVariableToView('item', View::newInstance()->_current('latestItems'));
        }
        return $item;
    }



    define("CBAY_ITEM_ROOT_ID", 1);
    define("CBAY_SERVICE_ROOT_ID", 2);


    require_once("frm/CBayItem.form.class.php");
    
?>
