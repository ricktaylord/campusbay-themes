<?php
    /*
     *      OSCLass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2010 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    osc_show_widgets('footer');
?>
<!-- footer -->
<footer>
		<nav>
		    <?php  osc_get_non_empty_categories(); 

			   cbay_sort_subcategories(); 
                    	  ?>
                    <?php
                        if(osc_count_categories () > 0) {
                            echo '<ul class="categories">';
                        }
                    ?>
                    <?php while ( osc_has_categories() ) { ?>
                        <li class="category">
                            <h1><strong><a class="category <?php echo osc_category_slug() ; ?>" href="<?php echo osc_search_category_url() ; ?>"><?php echo osc_category_name() ; ?></a> <span>(<?php echo osc_category_total_items() ; ?>)</span></strong></h1>
                            <?php if ( osc_count_subcategories() > 0 ) { ?>
                                <ul>
				    <?php $j=0 ?>
                                    <?php while ( osc_has_subcategories() ) { ?>
				        <?php if($j<5) { ?>
                                        <li class="subcategory"><a class="category <?php echo osc_category_slug() ; ?>" href="<?php echo osc_search_category_url() ; ?>"><?php echo osc_category_name() ; ?></a> <span>(<?php echo osc_category_total_items() ; ?>)</span></li>
					<?php } ?>
					<?php $j++; ?>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                                            <?php } ?>
	<li>
	<h1><strong>
        <a href="<?php echo osc_contact_url(); ?>"><?php _e('Contact', 'modern') ; ?></a> 
	</strong></h1>
	<ul>
        <?php osc_reset_static_pages() ; ?>
        <?php while( osc_has_static_pages() ) { ?>
	    <li>
            <a href="<?php echo osc_static_page_url() ; ?>"><?php echo osc_static_page_title() ; ?></a> 
	    </li>
        <?php } ?>
	</ul>
	</li>
	</nav>
</footer>
<!-- /footer -->
</div>
<!-- /container -->
<?php osc_run_hook('footer') ; ?>
