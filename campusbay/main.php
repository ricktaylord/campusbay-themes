<?php
    /*
     *      OSCLass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2010 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="<?php echo str_replace('_', '-', osc_current_user_locale()); ?>">
    <head>
        <?php osc_current_web_theme_path('head.php') ; ?>
        <meta name="robots" content="index, follow" />
        <meta name="googlebot" content="index, follow" />

	<script type="text/javascript" >

	setInterval("uniform_plugins()", 250);
            function uniform_plugins() {
                
                var content_plugin_hook = $('#plugin-hook').text();
                content_plugin_hook = content_plugin_hook.replace(/(\r\n|\n|\r)/gm,"");
                if( content_plugin_hook != '' ){
                    
                    var div_plugin_hook = $('#plugin-hook');
                    var num_uniform = $("div[id*='uniform-']", div_plugin_hook ).size();
                    if( num_uniform == 0 ){
                        if( $('#plugin-hook input:text').size() > 0 ){
                            $('#plugin-hook input:text').uniform();
                        }
                        if( $('#plugin-hook select').size() > 0 ){
                            $('#plugin-hook select').uniform();
                        }
                    }
                }
            }
        </script>

    </head> 

    <body>

        <?php osc_current_web_theme_path('header.php') ; ?>
        <div id="form_publish">
            <?php osc_current_web_theme_path('inc.search.php') ; ?>
        </div>
        <div class="content home">
            <div id="main">
		<div class="sell_stuff">
		<?php if( osc_is_web_user_logged_in() ) { ?>
		

		<?php $locales = osc_get_locales() ?>
		<form action="/index.php" method="post">
		<div class="col1">
		<h3>Post up an item you want to sell</h3>
		<input type="hidden" name="action" value="item_add_post" />
                <input type="hidden" name="page" value="item" />
		<?php require_once("inc.item_post_1.php"); ?>
		</div>
		<div class="col2">
		    <?php require_once("inc.item_post_2.php"); ?>

		</form>
		</div>
		<?php } else { ?>


		<div class="col1">
			<h3>Sell your stuff to other students</h3>
			<ol>
				<li>Fill in the details</li>
				<li>Post it on the site</li>
				<li>Wait for a buyer</li>
				<li>Sort out the deal by email</li>
				<li>Meet up</li>
			</ol>
		</div>

		<?php Session::newInstance()->_setReferer("/" ); ?>
		<form class="col2" action="<?php echo osc_user_login_url(true) ; ?>" method="post">
		<h2>Log in to get started</h2>

		<fieldset>
                                <input type="hidden" name="action" value="login_post" />
                                <label for="email"><?php _e('E-mail', 'campusbay') ; ?></label><br/>
                                <?php UserForm::email_login_text() ; ?><br/>
                                <label for="password"><?php _e('Password', 'campusbay') ; ?></label><br/>
                                <?php UserForm::password_login_text() ; ?>
                                <p class="checkbox"><?php UserForm::rememberme_login_checkbox();?> <label for="rememberMe"><?php _e('Remember me', 'campusbay') ; ?></label></p>
                                <button type="submit"><?php _e('Log in', 'campusbay') ; ?></button>
					<?php if(osc_user_registration_enabled()) { ?>
                            			&middot;
                            			<a href="<?php echo osc_register_account_url() ; ?>"><?php _e('Register for a free account', 'campusbay'); ?></a>
                        		<?php }; ?>
                            </fieldset>
                        </form>
	
		<?php } ?>
		    <div class="clear">&nbsp;</div>
		</div>
		<div class="latest">
			

			<div class="items">
			<h3><a href="<?php echo cbay_item_post_url() ?>">Sell your stuff</a></h3>
			<?php View::newInstance()->_erase('latestItems') ; ?>
			<?php if( cbay_count_latest_items(5, cbay_item_category_id()) == 0) { ?>
                        <p class="empty"><?php _e('No Latest Items', 'campusbay') ; ?></p>
                    <?php } else { ?>
			<h4>Latest</h4>
			<ul>
                                <?php $class = "even"; ?>
                                <?php while ( cbay_has_latest_items() ) { ?>
                                 <li class="<?php echo $class. (osc_item_is_premium()?" premium":"") ; ?>">
                                        <?php if( osc_images_enabled_at_items() ) { ?>
                                            <?php if( osc_count_item_resources() ) { ?>
                                                <a href="<?php echo osc_item_url() ; ?>">
                                                    <img src="<?php echo osc_resource_thumbnail_url() ; ?>" width="75" height="56" title="<?php echo osc_item_title(); ?>" alt="<?php echo osc_item_title(); ?>" />
                                                </a>
                                            <?php } else { ?>
                                                <img src="<?php echo osc_current_web_theme_url('images/no_photo.gif') ; ?>" alt="" title=""/>
                                            <?php } ?>
                                        <?php } ?>
					     <div class="summary">
					     <span class="title">
                                             <a href="<?php echo osc_item_url() ; ?>"><?php echo osc_item_title() ; ?></a>
					     </span>
                                             <span class="price"><strong><?php if( osc_price_enabled_at_items() ) { echo '&pound;'.osc_item_formated_price() ; ?>  <?php } ?>  </strong></span>
                                     </li>
                                    <?php $class = ($class == 'even') ? 'odd' : 'even' ; ?>
                                <?php } ?>
                        </ul>
                        <?php if( osc_count_latest_items() == osc_max_latest_items() ) { ?>
                        <p class='pagination'><?php echo osc_search_pagination(); ?></p>
                            <p class="see_more_link"><a href="<?php echo osc_search_show_all_url();?>"><strong><?php _e("See all offers", 'campusbay'); ?> &raquo;</strong></a></p>
                        <?php } ?>
                    <?php View::newInstance()->_erase('items') ; } ?>

			</div>
			<div class="services">
				<h3><a href="<?php echo cbay_service_post_url() ?>">Sell your services</a></h3>
			<?php View::newInstance()->_erase('latestItems') ; ?>
			<?php if( cbay_count_latest_items(5, cbay_service_category_id()) == 0) { ?>
                        <p class="empty"><?php _e('No Latest Items', 'campusbay') ; ?></p>
                    <?php } else { ?>
			<h4>Latest</h4>
			<ul>
                                <?php $class = "even"; ?>
                                <?php while ( cbay_has_latest_items() ) { ?>
                                 <li class="<?php echo $class. (osc_item_is_premium()?" premium":"") ; ?>">
					     <div class="summary">
					     <span class="title">
                                             <a href="<?php echo osc_item_url() ; ?>"><?php echo osc_item_title() ; ?></a>
					     </span>
                                             <span class="price"><strong><?php if( osc_price_enabled_at_items() ) { echo '&pound;'.osc_item_formated_price()."/ hr" ; ?>  <?php } ?>  </strong></span>
                                     </li>
                                    <?php $class = ($class == 'even') ? 'odd' : 'even' ; ?>
                                <?php } ?>
                        </ul>
                        <?php if( osc_count_latest_items() == osc_max_latest_items() ) { ?>
                        <p class='pagination'><?php echo osc_search_pagination(); ?></p>
                            <p class="see_more_link"><a href="<?php echo osc_search_show_all_url();?>"><strong><?php _e("See all offers", 'campusbay'); ?> &raquo;</strong></a></p>
                        <?php } ?>
                    <?php View::newInstance()->_erase('items') ; } ?>


			</div>
			<div class="clear">&nbsp;</div>
		</div>
        </div>
        <?php osc_current_web_theme_path('footer.php') ; ?>
    </body>
</html>
