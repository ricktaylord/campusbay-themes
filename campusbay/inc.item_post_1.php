			<div class="row">
                         <label for="catId"><?php _e('Category', 'campusbay'); ?></label>
                            <?php cbay_first_category_select("catId"); ?>
		</div>
		<div class="row">
			    <label for="title">Name / title</label>
                            <?php ItemForm::title_input("title"); ?>
                    <?php if( osc_price_enabled_at_items() ) { ?>
		</div>
		<div class="row">
                        <label for="price"><?php _e('Price', 'campusbay'); ?></label>
                        <?php ItemForm::price_input_text(); ?>
		    <?php } ?>
		</div>

